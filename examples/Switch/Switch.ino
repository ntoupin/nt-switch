/*
 * @file Led.ino
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Simple example of led utilisation.
 */

#include <NT_Switch.h>

Switch_t Switch_1;

void setup()
{
  Switch_1.Configure(FALSE);
  Switch_1.Attach(9);
  Switch_1.Init();

  pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
  digitalWrite(LED_BUILTIN, Switch_1.Get());
  delay(100);
}
