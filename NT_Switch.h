/*
 * @file NT_Switch.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Switch function declarations.
 */

#ifndef NT_Switch_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Switch_h

class Switch_t
{
public:
	Switch_t();
	bool Enable = FALSE;
	bool Value_Sp = FALSE;
	bool Value_Op = FALSE;
	bool Value_Op_Changed = FALSE;
	void Configure(bool Startup_Sp);
	void Attach(int Pin);
	void Init();
	void Deinit();
	bool Get();

private:
	int _Pin;
	bool _Startup_Sp = FALSE;
	bool _Value_Sp_Last = TRUE;
};

#endif