/*
 * @file NT_Switch.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Switch function definitions.
 */

#include "Arduino.h"
#include "NT_Switch.h"

Switch_t::Switch_t()
{
}

void Switch_t::Configure(bool Startup_Sp)
{
	_Startup_Sp = Startup_Sp;
}

void Switch_t::Attach(int Pin)
{
	_Pin = Pin;
	pinMode(_Pin, INPUT);
}

void Switch_t::Init()
{
	Enable = TRUE;
	Value_Sp = _Startup_Sp;
	_Value_Sp_Last = _Startup_Sp;
}

void Switch_t::Deinit()
{
	Value_Sp = _Startup_Sp;
	Enable = FALSE;
}

bool Switch_t::Get()
{
	if (Enable == TRUE)
	{
		int Value_Sp = digitalRead(_Pin);

		if (Value_Sp != _Value_Sp_Last)
		{
			Value_Op = Value_Sp;
			Value_Op_Changed = TRUE;
		}
		else
		{
			Value_Op_Changed = FALSE;
		}

		_Value_Sp_Last = Value_Sp;
	}
	else
	{
		// Do nothing
	}

	return Value_Op;
}